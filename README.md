[![pipeline status](https://gitlab.com/tdd-en-nodejs-guia-de-tests-con-jest/test-tdd/badges/master/pipeline.svg)](https://gitlab.com/tdd-en-nodejs-guia-de-tests-con-jest/test-tdd/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=%40apecr%2Ftest-tdd&metric=alert_status)](https://sonarcloud.io/dashboard?id=%40apecr%2Ftest-tdd)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=%40apecr%2Ftest-tdd&metric=coverage)](https://sonarcloud.io/dashboard?id=%40apecr%2Ftest-tdd)

# TEST TDD

Repo for Test TDD with Nodejs [course][1].

[1]: https://www.udemy.com/course/tdd-en-nodejs-guia-de-tests-con-jest/