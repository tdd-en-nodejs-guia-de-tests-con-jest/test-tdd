const { users } = require('../../src/endpoints/index')

describe('Testing all the endpoints in the API', () => {
  const axios = {
    get: jest.fn().mockResolvedValue({ data: 1 }),
    post: jest.fn().mockResolvedValue({ data: 1 }),
    put: jest.fn().mockResolvedValue({ data: 1 }),
    delete: jest.fn().mockResolvedValue({ data: 1 })
  }
  describe('users endpoints', () => {
    describe('get the users', () => {
      it('Returns the user json correctly', async() => {
        const resp = {
          status: jest.fn().mockReturnThis(),
          send: jest.fn()
        }
        await users({ axios }).get({}, resp)
        expect(resp.status.mock.calls).toEqual([ [ 200 ] ])
        expect(resp.send.mock.calls).toEqual([ [ 1 ] ])
      })
    })
    describe('post a user', () => {
      it('Post a user', async() => {
        const resp = {
          status: jest.fn().mockReturnThis(),
          send: jest.fn()
        }
        const request = {
          body: 'body request'
        }
        await users({axios}).post(request, resp)
        expect(resp.status.mock.calls).toEqual([ [ 201 ] ])
        expect(resp.send.mock.calls).toEqual([ [ 1 ] ])
        expect(axios.post.mock.calls).toEqual([ ['https://jsonplaceholder.typicode.com/users', request.body] ])
      })
    })
    describe('put a user', () => {
      it('Put a user', async() => {
        const id = 1
        const putBody = 'body request for put'
        const resp = {
          sendStatus: jest.fn(),
        }
        const request = {
          params: {
            id
          },
          body: putBody
        }
        await users({axios}).put(request, resp)
        expect(resp.sendStatus.mock.calls).toEqual([ [ 204 ] ])
        expect(axios.put.mock.calls).toEqual(
          [
            [`https://jsonplaceholder.typicode.com/users/${id}`,
              putBody
            ]
          ])
      })
    })
  })
  describe('delete a user', () => {
    it('Delete a user', async() => {
      const id = 120
      const resp = {
        sendStatus: jest.fn(),
      }
      const request = {
        params: {
          id
        }
      }
      await users({axios}).delete(request, resp)
      expect(resp.sendStatus.mock.calls).toEqual([ [ 204 ] ])
      expect(axios.delete.mock.calls).toEqual(
        [
          [ `https://jsonplaceholder.typicode.com/users/${id}` ]
        ])
    })
  })
})