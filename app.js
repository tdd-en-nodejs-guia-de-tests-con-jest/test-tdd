const express = require('express')
const services = require('./services')
const parser = require('body-parser')
const { users, posts } = require('./src/endpoints')
const authenticate = require('./src/middlewares/authenticate')

const port = 3000
const app = express()

app.use(parser.urlencoded({ extended: false }))
app.use(parser.json())

const userHandlers = users(services)
const postHandler = posts(services)

app.get('/', userHandlers.get)

app.post('/', userHandlers.post)

app.put('/:id', userHandlers.put)

app.delete('/:id', userHandlers.delete)

app.post('/posts', authenticate, postHandler.post)

app.listen(port, () => console.log(`Example app listening on port ${port}`))

module.exports = app